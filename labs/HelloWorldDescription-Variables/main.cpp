/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 10:33 AM
 */

#include <cstdlib> // cstdlib libary not needed
#include <iostream> // iostream is needed for output

//Gives the context of where my libraries are coming from
using namespace std;

/*
 * 
 */
// There is always and only one main
// Programs always start at main
// Programs execute from top to bottom, left to right
int main(int argc, char** argv) {
   
    // cout specifies output
    // endl creates a new line
    // << specifies stream operator
    // All statements en in a semicolon
    
    // Using a programmer-defined identifier/variable
    // string is a data type
    // = is an assignment operator
    // Assign right to left 
    // string message = "Hello World";
    string message; // Variable declaration
    message = "Hello World"; // variable initialization 
    
    // Prompt the user 
    cout << "please enter a message" << endl;
    
    cin >> message; // user input
    
    cout << "your message is " << message << endl;
    
    
    cout << "Hello World" << endl;
    cout 
            << "Christopher Cervantes";
    
    
    // If program hits return, it ran successfully
    return 0;
} // All my code is within curly braces

